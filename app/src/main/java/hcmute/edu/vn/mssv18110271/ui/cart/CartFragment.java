package hcmute.edu.vn.mssv18110271.ui.cart;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.Cart;
import hcmute.edu.vn.mssv18110271.database.Model.OrderDetailModel;
import hcmute.edu.vn.mssv18110271.database.Model.ProductModel;
import hcmute.edu.vn.mssv18110271.ui.saleorder.SaleOrderActivity;

public class CartFragment extends Fragment {

    private CartViewModel mViewModel;
    public TextView txtTongTien;
    public Button btnThanhToan;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mViewModel =
                new ViewModelProvider(this).get(CartViewModel.class);
        View root = inflater.inflate(R.layout.fragment_cart, container, false);
        txtTongTien = (TextView)  root.findViewById(R.id.txtTongTien);
        RecyclerView rvCarts = (RecyclerView) root.findViewById(R.id.rvCart);

        Database database = new Database(getContext(), Database.DATABASE_NAME, null);
        List<OrderDetailModel> dbcarts = Cart.CURRENT_CART;
        CartAdapter adapter = new CartAdapter(dbcarts,txtTongTien);

        rvCarts.setAdapter(adapter);

        rvCarts.setLayoutManager(new LinearLayoutManager(getContext()));
        int curSize = adapter.getItemCount();

        btnThanhToan = (Button) root.findViewById(R.id.btnThanhtoan);
        btnThanhToan.setOnClickListener(view -> {
            List<OrderDetailModel> cart = Cart.CURRENT_CART;
            if(cart.size()>0){
                Intent intent = new Intent(getContext(), SaleOrderActivity.class);
                startActivity(intent);
                Toast.makeText(view.getContext(), "Vui lòng kiểm tra đơn hàng của bạn", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(view.getContext(), "Giỏ hàng phải có ít nhất một sản phẩm", Toast.LENGTH_SHORT).show();
            }


        });
        txtTongTien.setText(String.valueOf(getTotalCost()));
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        txtTongTien.setText(String.valueOf(getTotalCost()));
    }
    public void setTongTien(){
        txtTongTien.setText(String.valueOf(getTotalCost()));
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(CartViewModel.class);
        // TODO: Use the ViewModel
    }
    public int getTotalCost(){
        int total = 0;
        for (OrderDetailModel order : Cart.CURRENT_CART){
            Database dtb = new Database(getContext() , Database.DATABASE_NAME, null);
            ProductModel prd = dtb.getByIdProduct(order.getProductId());
            total = total + prd.getPrice()*order.getQuantity();
        }
        return total;
    }

}