package hcmute.edu.vn.mssv18110271;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.UserModel;

public class ForgetPassword_CreateNewPassword extends AppCompatActivity {
    public EditText txtPass1;
    public EditText txtPass2;
    public Button btnSend;
    public TextView txtBack;
    public EditText txtOtp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password__create_new_password);

        txtPass1 = (EditText) findViewById(R.id.txtNewPasswordForget1);
        txtPass2 = (EditText) findViewById(R.id.txtNewPasswordForget2);
        btnSend = (Button) findViewById(R.id.btnSendPasswordForget);
        txtBack = (TextView) findViewById(R.id.txtCreateBackToLogin);
        txtOtp = (EditText) findViewById(R.id.txtOTP);
        btnSend.setOnClickListener(view -> {
            SharedPreferences sharedPreferences= this.getSharedPreferences("MiniStop", Context.MODE_PRIVATE);
            if(sharedPreferences != null){
                String code = sharedPreferences.getString("Code", "123456");
                Log.d("code", code);
                if(txtOtp.getText().toString().equals(code) == false){
                    Toast.makeText(this,"Mã xác nhận của bạn không đúng", Toast.LENGTH_SHORT);
                    return;
                }
            }
            String email = getIntent().getStringExtra("email");
            Database database = new Database(this, Database.DATABASE_NAME, null);
            UserModel user = database.getUserByEmail(email);
            if(user != null){
                user.setPassword(txtPass2.getText().toString());

                database.updateUser(user);
            }

            Toast.makeText(this,"Đổi mật khẩu thành công", Toast.LENGTH_SHORT);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        });
        txtBack.setOnClickListener(view -> {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Quên mật khẩu");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
}