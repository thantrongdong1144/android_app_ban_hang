package hcmute.edu.vn.mssv18110271.ui.saleHistory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.OrderDetailModel;
import hcmute.edu.vn.mssv18110271.database.Model.OrderModel;
import hcmute.edu.vn.mssv18110271.database.Model.ProductModel;

public class SaleHistoryAdapter extends RecyclerView.Adapter<SaleHistoryAdapter.ViewHolder> {
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameOrder;
        public TextView dateOrder;
        public TextView totalOrder;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameOrder = (TextView) itemView.findViewById(R.id.txtItemSaleHisName);
            dateOrder = (TextView) itemView.findViewById(R.id.txtItemSaleHisDate);
            totalOrder = (TextView) itemView.findViewById(R.id.txtItemSaleHisTotal);
        }
    }
    // Store a member variable for the contacts
    private List<OrderModel> mOrder;
    private Context mContext;
    // Pass in the contact array into the constructor
    public SaleHistoryAdapter(List<OrderModel> products) {
        mOrder = products;
    }
    @Override
    public SaleHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View productView = inflater.inflate(R.layout.item_sale_history, parent, false);

        // Return a new holder instance
        SaleHistoryAdapter.ViewHolder viewHolder = new SaleHistoryAdapter.ViewHolder(productView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(SaleHistoryAdapter.ViewHolder holder, int position) {
        // Get the data model based on position
        OrderModel detail = mOrder.get(position);
        // Set item views based on your views and data model
        holder.nameOrder.setText(detail.getName());

        holder.dateOrder.setText(detail.getDateCreated());
        holder.totalOrder.setText(String.valueOf(detail.getTotal()) );

    }
    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mOrder.size();
    }
}