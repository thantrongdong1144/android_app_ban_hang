package hcmute.edu.vn.mssv18110271;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.UserModel;

public class LoginActivity extends AppCompatActivity {
    public Button loginBtn;
    public EditText emailText;
    public EditText passwordText;
    public  Button btnRegister;
    public TextView txtForgetPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailText = (EditText) findViewById(R.id.txtEmailLog);
        passwordText = (EditText) findViewById(R.id.txtPasswordLog);
        txtForgetPass = (TextView) findViewById(R.id.txtForgetPass) ;
        loginBtn = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnLoginRegister) ;
        txtForgetPass.setOnClickListener(view -> {
            Intent intent = new Intent(LoginActivity.this , ForgetPasswordActivity.class);
            startActivity(intent);
        });
        loginBtn.setOnClickListener(v -> {
            String email = emailText.getText().toString();
            String password = passwordText.getText().toString();

            Database dtb = new Database(getApplicationContext(),Database.DATABASE_NAME,null);
            UserModel user = dtb.userValidation(email,password);

            if(user!= null){
                UserModel.CURRENT_USER = user;
                if(user.getRole().trim().equals("admin")){

                }
                else if(user.getRole().trim().equals("user")){
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(intent);
                }
            }

        });
        btnRegister.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Đăng nhập");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
}