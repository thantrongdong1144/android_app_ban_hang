package hcmute.edu.vn.mssv18110271.ui.saleorder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.Cart;
import hcmute.edu.vn.mssv18110271.database.Model.OrderDetailModel;
import hcmute.edu.vn.mssv18110271.database.Model.ProductModel;
import hcmute.edu.vn.mssv18110271.ui.cart.CartAdapter;

public class SaleOrderAdapter extends RecyclerView.Adapter<SaleOrderAdapter.ViewHolder> {
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameProduct;
        public TextView costProduct;
        public TextView quantity;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameProduct = (TextView) itemView.findViewById(R.id.txtItemSaleName);
            costProduct = (TextView) itemView.findViewById(R.id.txtItemSalePrice);
            quantity = (TextView) itemView.findViewById(R.id.txtItemSaleQuantity);
        }
    }
    // Store a member variable for the contacts
    private List<OrderDetailModel> mCart;
    private Context mContext;
    // Pass in the contact array into the constructor
    public SaleOrderAdapter(List<OrderDetailModel> products) {
        mCart = products;
    }
    @Override
    public SaleOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View productView = inflater.inflate(R.layout.item_sale_order, parent, false);

        // Return a new holder instance
        SaleOrderAdapter.ViewHolder viewHolder = new SaleOrderAdapter.ViewHolder(productView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(SaleOrderAdapter.ViewHolder holder, int position) {
        // Get the data model based on position
        OrderDetailModel detail = mCart.get(position);
        Database database = new Database(mContext, Database.DATABASE_NAME, null);
        ProductModel product = database.getByIdProduct(detail.getProductId());
        // Set item views based on your views and data model
        TextView nameproduct = holder.nameProduct;
        nameproduct.setText(product.getName());
        TextView costproduct = holder.costProduct;
        int cost = product.getPrice() * detail.getQuantity();
        costproduct.setText(String.valueOf(cost));
        TextView quantity = holder.quantity;
        quantity.setText(detail.getQuantity().toString());

    }
    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mCart.size();
    }
}