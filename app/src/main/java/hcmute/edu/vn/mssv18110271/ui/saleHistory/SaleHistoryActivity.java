package hcmute.edu.vn.mssv18110271.ui.saleHistory;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.Cart;
import hcmute.edu.vn.mssv18110271.database.Model.OrderDetailModel;
import hcmute.edu.vn.mssv18110271.database.Model.OrderModel;
import hcmute.edu.vn.mssv18110271.database.Model.UserModel;
import hcmute.edu.vn.mssv18110271.ui.saleorder.SaleOrderAdapter;

public class SaleHistoryActivity extends AppCompatActivity {
    public TextView nameCus;
    public  TextView pointCus;
    public TextView backToLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_history);

        RecyclerView rvOrder = (RecyclerView) findViewById(R.id.rcvListItemHisOrder);

        Database database = new Database(this, Database.DATABASE_NAME, null);
        UserModel user = UserModel.CURRENT_USER;
        nameCus = (TextView) findViewById(R.id.txtHisOrderName);
        pointCus = (TextView) findViewById(R.id.txtHisOrderPoint);
        List<OrderModel> mOrders;
        if(user != null){
            mOrders = database.getByIdUser(user.getId());
            nameCus.setText(user.getFullName());
            pointCus.setText(String.valueOf(user.getPoint()));
        } else {
            mOrders = database.getByIdUser(1);
            nameCus.setText("Than Trong Dong");
            pointCus.setText("100");
        }
        SaleHistoryAdapter adapter = new SaleHistoryAdapter(mOrders);

        rvOrder.setAdapter(adapter);

        rvOrder.setLayoutManager(new LinearLayoutManager(this));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Lịch sử mua hàng");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
}