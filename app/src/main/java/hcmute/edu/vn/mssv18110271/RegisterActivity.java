package hcmute.edu.vn.mssv18110271;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.ByteArrayOutputStream;

import hcmute.edu.vn.mssv18110271.database.Database;

public class RegisterActivity extends AppCompatActivity {
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    public Button btnTakePic;
    public Button btnRegister;
    public Bitmap picture;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnTakePic = (Button) findViewById(R.id.btnTakePic);
        btnTakePic.setOnClickListener((view)->{
            take_Pic(view);
        });

        Database dtb = new Database(getApplicationContext(), Database.DATABASE_NAME, null);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        EditText emailView = (EditText) findViewById(R.id.txtEmailRes);
        EditText passView = (EditText) findViewById(R.id.txtPasswordRes);
        EditText nameView = (EditText) findViewById(R.id.txtNameRes) ;

        btnRegister.setOnClickListener((view) -> {
            String email = emailView.getText().toString();
            String pass = passView.getText().toString();
            String name = nameView.getText().toString();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            picture.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            if(!dtb.checkUserExist(email)){
                dtb.userCreate(email, pass, name, "user",byteArray);

                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }


        });
    }

    public void take_Pic(final View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {

                Bitmap myBitmap = data.getExtras().getParcelable("data");
                picture = myBitmap;
                ImageView photo = (ImageView) findViewById(R.id.imgViewAvatar);
                photo.setImageBitmap(myBitmap);// here I am setting the pic to an image view for the user to have a look.

            }

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setTitle("Đăng kí");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
}