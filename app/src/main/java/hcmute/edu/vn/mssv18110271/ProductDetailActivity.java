package hcmute.edu.vn.mssv18110271;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.Cart;
import hcmute.edu.vn.mssv18110271.database.Model.OrderDetailModel;
import hcmute.edu.vn.mssv18110271.database.Model.ProductModel;

public class ProductDetailActivity extends AppCompatActivity {
    public TextView txtPrdName;
    public TextView txtPrdPrice;
    public TextView txtPrdDes;
    public Button btnBuyNow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        txtPrdName = findViewById(R.id.txtPrdDetailName);
        txtPrdPrice = findViewById(R.id.txtPrdDetailPrice);
        txtPrdDes = findViewById(R.id.txtPrdDetailDes);
        btnBuyNow = findViewById(R.id.btnPrdDetailBuyNow);

        Integer productId = getIntent().getIntExtra("PRODUCT_ID",1);
        Database database = new Database(this, Database.DATABASE_NAME, null);
        ProductModel prd = database.getByIdProduct(productId);

        if(prd != null){
            txtPrdName.setText(prd.getName().toString());
            txtPrdPrice.setText(prd.getPrice().toString());
            txtPrdDes.setText(prd.getDescription().toString());
            btnBuyNow.setOnClickListener(view->{
                List<OrderDetailModel> cart = Cart.CURRENT_CART;
                boolean check = false;
                for (OrderDetailModel order : cart){
                    if(order.getProductId() == prd.getId()){
                        order.setQuantity(order.getQuantity() + 1);
                        check = true;
                    }
                }
                if(check == false){
                    OrderDetailModel detail = new OrderDetailModel(1,prd.getId(),1,prd.getPrice());
                    cart.add(detail);
                }
                Cart.CURRENT_CART = cart;
//            Toast.makeText(view.getContext(), String.valueOf(Cart.CURRENT_CART.size()), Toast.LENGTH_SHORT).show();
                Toast.makeText(view.getContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
            });
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Chi tiết sản phẩm");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
}