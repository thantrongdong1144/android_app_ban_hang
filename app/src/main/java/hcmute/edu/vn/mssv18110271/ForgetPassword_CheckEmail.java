package hcmute.edu.vn.mssv18110271;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ForgetPassword_CheckEmail extends AppCompatActivity {
    public Button btnResend;
    public Button btnOk;
    public TextView txtBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password__check_email);

        btnResend = (Button) findViewById(R.id.btnResendEmailForget);
        btnOk = (Button) findViewById(R.id.btnCheckEmailForgetOK);
        txtBack = (TextView) findViewById(R.id.txtCheckEmailBacktoLogin);

        btnResend.setOnClickListener(view -> {
            Toast.makeText(this,"Vui lòng kiểm tra email của bạn", Toast.LENGTH_SHORT);
        });
        btnOk.setOnClickListener(view -> {
            String email = getIntent().getStringExtra("email");
            Intent intent = new Intent(this, ForgetPassword_CreateNewPassword.class);
            intent.putExtra("email", email);
            startActivity(intent);
        });
        txtBack.setOnClickListener(view -> {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Quên mật khẩu");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
}