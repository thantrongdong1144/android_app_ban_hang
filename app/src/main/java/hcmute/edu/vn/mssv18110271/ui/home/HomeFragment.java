package hcmute.edu.vn.mssv18110271.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import hcmute.edu.vn.mssv18110271.LoginActivity;
import hcmute.edu.vn.mssv18110271.MainActivity;
import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Model.Cart;
import hcmute.edu.vn.mssv18110271.ui.productInCategory.SearchByCategoryActivity;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    public Button btnCtgSandwich;
    public Button btnCtgNuocGiaiKhat;
    public Button btnCtgSushi;
    public Button btnCtgOnigiri;
    public Button btnCtgTrangMieng;
    public Button btnCtgCom;
    public Button btnCtgOden;
    public Button btnCtgThucAnNhanh;
    public Button btnCtgStreetFood;
    public Button btnCtgSalad;
    public Button btnCtgMi;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        btnCtgSandwich = (Button) root.findViewById(R.id.btnCtgSandwich);
        btnCtgNuocGiaiKhat = (Button) root.findViewById(R.id.btnCtgNuocGiaiKhat);
        btnCtgSushi = (Button) root.findViewById(R.id.btnCtgSushi);
        btnCtgOnigiri = (Button) root.findViewById(R.id.btnCtgOnigiri);
        btnCtgTrangMieng = (Button) root.findViewById(R.id.btnCtgTrangMieng);
        btnCtgCom = (Button) root.findViewById(R.id.btnCtgCom);
        btnCtgOden = (Button) root.findViewById(R.id.btnCtgOden);
        btnCtgThucAnNhanh = (Button) root.findViewById(R.id.btnCtgThucAnNhanh);
        btnCtgStreetFood = (Button) root.findViewById(R.id.btnCtgStreetFood);
        btnCtgSalad = (Button) root.findViewById(R.id.btnCtgSalad);
        btnCtgMi = (Button) root.findViewById(R.id.btnCtgMi);
        btnCtgSandwich.setOnClickListener((view)->{
//            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 1);
            startActivity(intent);
        });
        btnCtgNuocGiaiKhat.setOnClickListener((view)->{
//            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 2);
            startActivity(intent);
        });
        btnCtgSushi.setOnClickListener((view)->{
//            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 3);
            startActivity(intent);
        });
        btnCtgOnigiri.setOnClickListener((view)->{
//            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 4);
            startActivity(intent);
        });
        btnCtgTrangMieng.setOnClickListener((view)->{
            //            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 5);
            startActivity(intent);
        });
        btnCtgCom.setOnClickListener((view)->{
            //            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 6);
            startActivity(intent);
        });
        btnCtgOden.setOnClickListener((view)->{
            //            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 7);
            startActivity(intent);
        });
        btnCtgThucAnNhanh.setOnClickListener((view)->{
            //            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 8);
            startActivity(intent);
        });
        btnCtgStreetFood.setOnClickListener((view)->{
            //            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 9);
            startActivity(intent);
        });
        btnCtgSalad.setOnClickListener((view)->{
            //            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 10);
            startActivity(intent);
        });
        btnCtgMi.setOnClickListener((view)->{
            //            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(inflater.getContext(), SearchByCategoryActivity.class);
            intent.putExtra("CATEGORY_ID", 11);
            startActivity(intent);
        });

        return root;
    }
}