package hcmute.edu.vn.mssv18110271.ui.cart;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.Cart;
import hcmute.edu.vn.mssv18110271.database.Model.OrderDetailModel;
import hcmute.edu.vn.mssv18110271.database.Model.OrderModel;
import hcmute.edu.vn.mssv18110271.database.Model.ProductModel;
import hcmute.edu.vn.mssv18110271.ui.product.ProductAdapter;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameProduct;
        public TextView costProduct;
        public Button btnCong;
        public Button btnTru;
        public TextView quantity;
        public Button btnXoa;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameProduct = (TextView) itemView.findViewById(R.id.txtItemCartName);
            costProduct = (TextView) itemView.findViewById(R.id.txtItemCartCost);
            btnCong = (Button) itemView.findViewById(R.id.btnCong);
            btnTru = (Button) itemView.findViewById(R.id.btnTru);
            quantity = (TextView) itemView.findViewById(R.id.txtCartQuantity);
            btnXoa = (Button) itemView.findViewById(R.id.btnDeleteItemCart);
        }
    }
    // Store a member variable for the contacts
    private List<OrderDetailModel> mCart;
    private TextView txtTong;
    private Context mContext;
    // Pass in the contact array into the constructor
    public CartAdapter(List<OrderDetailModel> products, TextView txtTongTien) {
        mCart = products;
        this.txtTong = txtTongTien;
    }
    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        mContext = context;
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View productView = inflater.inflate(R.layout.item_cart, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(productView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(CartAdapter.ViewHolder holder, int position) {
        // Get the data model based on position
        OrderDetailModel detail = mCart.get(position);
        Database database = new Database(mContext, Database.DATABASE_NAME, null);
        ProductModel product = database.getByIdProduct(detail.getProductId());
        // Set item views based on your views and data model
        TextView nameproduct = holder.nameProduct;
        nameproduct.setText(product.getName());
        TextView costproduct = holder.costProduct;
        costproduct.setText(product.getPrice().toString());
        TextView quantity = holder.quantity;
        quantity.setText(detail.getQuantity().toString());

        holder.btnCong.setOnClickListener(view->{
            List<OrderDetailModel> cart = Cart.CURRENT_CART;
            OrderDetailModel orderDetailModel = new OrderDetailModel();
            boolean check = false;
            for (OrderDetailModel order : cart){
                if(order.getProductId() == product.getId()){
                    order.setQuantity(order.getQuantity() + 1);
                    orderDetailModel = order;
                }
            }
            Cart.CURRENT_CART = cart;
            quantity.setText(orderDetailModel.getQuantity().toString());
            txtTong.setText(String.valueOf(getTotalCost()));
//            CartFragment.newInstance().setTongTien();
//            CartFragment cartFragment = new CartFragment();
//            cartFragment.setTongTien();
            Toast.makeText(view.getContext(), "Cập nhật thành công", Toast.LENGTH_SHORT).show();
        });
        holder.btnTru.setOnClickListener(view->{
            List<OrderDetailModel> cart = Cart.CURRENT_CART;
            OrderDetailModel orderDetailModel = new OrderDetailModel();
            boolean check = false;
            for (OrderDetailModel order : cart){
                if(order.getProductId() == product.getId()){
                    if(order.getQuantity() > 1){
                        order.setQuantity(order.getQuantity() - 1);
                        orderDetailModel = order;
                        quantity.setText(orderDetailModel.getQuantity().toString());
                        check = true;
                    } else {
                        removeItem(holder,position);
                        txtTong.setText(String.valueOf(getTotalCost()));
                        Toast.makeText(view.getContext(), "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                        break;
                    }

                }
            }
            if(check== true){
                Cart.CURRENT_CART = cart;
            }
//            CartFragment.newInstance().setTongTien();
            txtTong.setText(String.valueOf(getTotalCost()));
            Toast.makeText(view.getContext(), "Cập nhật thành công", Toast.LENGTH_SHORT).show();
//            Toast.makeText(view.getContext(), String.valueOf(Cart.CURRENT_CART.size()), Toast.LENGTH_SHORT).show();
        });
        holder.btnXoa.setOnClickListener((view)->{
            removeItem(holder,position);
            txtTong.setText(String.valueOf(getTotalCost()));
            Toast.makeText(view.getContext(), "Cập nhật thành công", Toast.LENGTH_SHORT).show();
        });

    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mCart.size();
    }
    private void removeItem(CartAdapter.ViewHolder holder, int position) {
        int newPosition = holder.getAdapterPosition();
        mCart.remove(newPosition);
        notifyItemRemoved(newPosition);
        notifyItemRangeChanged(newPosition, mCart.size());
    }
    public int getTotalCost(){
        int total = 0;
        for (OrderDetailModel order : Cart.CURRENT_CART){
            Database dtb = new Database(mContext , Database.DATABASE_NAME, null);
            ProductModel prd = dtb.getByIdProduct(order.getProductId());
            total = total + prd.getPrice()*order.getQuantity();
        }
        return total;
    }
}
