package hcmute.edu.vn.mssv18110271.ui.saleorder;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hcmute.edu.vn.mssv18110271.MainActivity;
import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.Cart;
import hcmute.edu.vn.mssv18110271.database.Model.OrderDetailModel;
import hcmute.edu.vn.mssv18110271.database.Model.OrderModel;
import hcmute.edu.vn.mssv18110271.database.Model.ProductModel;
import hcmute.edu.vn.mssv18110271.database.Model.UserModel;
import hcmute.edu.vn.mssv18110271.ui.cart.CartAdapter;

public class SaleOrderActivity extends AppCompatActivity {
    public TextView saleName;
    public TextView saleDate;
    public TextView saleTotal;
    public Button btnChotdon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_order);

        RecyclerView rvOrder = (RecyclerView) findViewById(R.id.rcvListItemSaleOrder);

        Database database = new Database(this, Database.DATABASE_NAME, null);
        List<OrderDetailModel> dbcarts = Cart.CURRENT_CART;
        SaleOrderAdapter adapter = new SaleOrderAdapter(dbcarts);

        rvOrder.setAdapter(adapter);

        rvOrder.setLayoutManager(new LinearLayoutManager(this));

        saleName = (TextView) findViewById(R.id.txtSaleOrderName);
        saleDate = (TextView) findViewById(R.id.txtSaleOrderDate);
        saleTotal = (TextView) findViewById(R.id.txtSaleOrderTotal);

        btnChotdon = (Button) findViewById(R.id.btnSaleOrderChotDon);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        btnChotdon.setOnClickListener(v -> {
            UserModel user = UserModel.CURRENT_USER;
            List<OrderDetailModel> cart = Cart.CURRENT_CART;
            OrderModel newOrder = new OrderModel();
            if(user != null){
                newOrder.setUserID(user.getId());
                newOrder.setName(user.getFullName());
            } else {
                newOrder.setUserID(1);
                newOrder.setName("Than Trong Dong");
            }
            newOrder.setStatus("done");
            newOrder.setTotal(getTotalCost());
            Date date = new Date();
            newOrder.setDateCreated(formatter.format(date));

            OrderModel order = database.createOrder(newOrder);
            for(OrderDetailModel details : cart){
                details.setOrderId(order.getId());
            }
            for(OrderDetailModel detail : cart){
                database.createOrderDetail(detail);
            }
            Cart.CURRENT_CART = new ArrayList<>();
            Toast.makeText(this, "Thanh toán thành công", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(SaleOrderActivity.this, MainActivity.class);
            startActivity(intent);
        });

        UserModel user = UserModel.CURRENT_USER;
        if(user != null){
            saleName.setText(user.getFullName());
        }
        Date date = new Date();

        saleDate.setText(formatter.format(date));

        saleTotal.setText(String.valueOf(getTotalCost()) + " VNĐ");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public int getTotalCost(){
        int total = 0;
        for (OrderDetailModel order : Cart.CURRENT_CART){
            Database dtb = new Database(this , Database.DATABASE_NAME, null);
            ProductModel prd = dtb.getByIdProduct(order.getProductId());
            total = total + prd.getPrice()*order.getQuantity();
        }
        return total;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Hóa đơn");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
}