package hcmute.edu.vn.mssv18110271;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import hcmute.edu.vn.mssv18110271.database.Model.UserModel;

public class ProfileActivity extends AppCompatActivity {
    public EditText txtName;
    public EditText txtEmail;
    public ImageView imgAvt;
    public Button btnLogout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txtName = (EditText) findViewById(R.id.txtNameProfile);
        txtEmail = (EditText) findViewById(R.id.txtEmailProfile);
        btnLogout = (Button) findViewById(R.id.btnLogoutProfile);
        imgAvt = (ImageView) findViewById(R.id.imgProfile);
        UserModel user = UserModel.CURRENT_USER;
        if(user != null){
            txtName.setText(user.getFullName());
            txtEmail.setText(user.getEmail());
            byte[] bitmapdata = user.getImage();
            Bitmap bitmap;
            if(bitmapdata != null){
                bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
                imgAvt.setImageBitmap(bitmap);
            }


        }
        btnLogout.setOnClickListener(v->{
            UserModel.CURRENT_USER = null;
            Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Thông tin cá nhân");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
}