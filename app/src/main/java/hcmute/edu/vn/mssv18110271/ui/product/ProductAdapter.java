package hcmute.edu.vn.mssv18110271.ui.product;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import hcmute.edu.vn.mssv18110271.ProductDetailActivity;
import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Model.Cart;
import hcmute.edu.vn.mssv18110271.database.Model.OrderDetailModel;
import hcmute.edu.vn.mssv18110271.database.Model.OrderModel;
import hcmute.edu.vn.mssv18110271.database.Model.ProductModel;
import hcmute.edu.vn.mssv18110271.ui.productInCategory.SearchByCategoryActivity;

public class ProductAdapter  extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameProduct;
        public TextView costProduct;
        public Button addToCart;
        public ImageView imgPrd;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameProduct = (TextView) itemView.findViewById(R.id.txtItemProductName);
            costProduct = (TextView) itemView.findViewById(R.id.txtItemProductCost);
            addToCart = (Button) itemView.findViewById(R.id.btnItemAddToCart);
            imgPrd = (ImageView) itemView.findViewById(R.id.imgItemProduct);
        }
    }
    // Store a member variable for the contacts
    private List<ProductModel> mProducts;

    // Pass in the contact array into the constructor
    public ProductAdapter(List<ProductModel> products) {
        mProducts = products;
    }
    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View productView = inflater.inflate(R.layout.item_product, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(productView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {
        // Get the data model based on position
        ProductModel product = mProducts.get(position);

        // Set item views based on your views and data model
        TextView nameproduct = holder.nameProduct;
        nameproduct.setText(product.getName());
        TextView costproduct = holder.costProduct;
        costproduct.setText(product.getPrice().toString());
        if(product.getImage() != null){
            byte[] bitmapdata = product.getImage();
            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
            holder.imgPrd.setImageBitmap(bitmap);
        }
        holder.itemView.setOnClickListener((view)->{
//            Toast.makeText(view.getContext(), "hello", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(view.getContext(), ProductDetailActivity.class);
            intent.putExtra("PRODUCT_ID", product.getId());
            view.getContext().startActivity(intent);
        });
        holder.addToCart.setOnClickListener((view)->{
            List<OrderDetailModel> cart = Cart.CURRENT_CART;
            boolean check = false;
            for (OrderDetailModel order : cart){
                if(order.getProductId() == product.getId()){
                    order.setQuantity(order.getQuantity() + 1);
                    check = true;
                }
            }
            if(check == false){
                OrderDetailModel detail = new OrderDetailModel(1,product.getId(),1,product.getPrice());
                cart.add(detail);
            }
            Cart.CURRENT_CART = cart;
//            Toast.makeText(view.getContext(), String.valueOf(Cart.CURRENT_CART.size()), Toast.LENGTH_SHORT).show();
            Toast.makeText(view.getContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();

        });

    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mProducts.size();
    }
}
