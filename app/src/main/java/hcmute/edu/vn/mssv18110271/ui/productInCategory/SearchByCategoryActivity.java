package hcmute.edu.vn.mssv18110271.ui.productInCategory;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import hcmute.edu.vn.mssv18110271.R;
import hcmute.edu.vn.mssv18110271.database.Database;
import hcmute.edu.vn.mssv18110271.database.Model.ProductModel;
import hcmute.edu.vn.mssv18110271.ui.product.ProductAdapter;

public class SearchByCategoryActivity extends AppCompatActivity {
    List<ProductModel> lSearchproducts;
    RecyclerView rvProducts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_category);
//        Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show();
        rvProducts = (RecyclerView) findViewById(R.id.rvSearchByCategory);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Integer category_id = getIntent().getIntExtra("CATEGORY_ID",1);

        Database database = new Database(this, Database.DATABASE_NAME, null);

        List<ProductModel> dbproducts = database.getProductByCategoryId(category_id);

        if(dbproducts.size() == 0){
            Toast.makeText(this, "Hiện không có sản phẩm nào được tìm thấy", Toast.LENGTH_SHORT).show();
        }

        lSearchproducts = dbproducts;
        SearchByCategoryAdapter adapter = new SearchByCategoryAdapter(lSearchproducts);
        rvProducts.setAdapter(adapter);
        rvProducts.setLayoutManager(new LinearLayoutManager(this));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Sản phẩm");
        if(bar != null){
            Log.d("color", "khac null");
//            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("@color/submenu")));
            bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.sub_menu));
        }else {
            Log.d("color", "null");
        }
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
}